(i) <~ [1, 2].forEach
return unless ig.containers["podilmaturantu-#i"]
container = d3.select ig.containers["podilmaturantu-#i"]
container.append \h3
  ..html "Podíl maturitního ročníku zapsaný na VŠ"
data = d3.csv.parse ig.data.podilmaturantu
scaleX = d3.scale.linear!
  ..domain [1999, 2014]
scaleY = d3.scale.linear!
  ..domain [0.65, 0]

podilmaturantu = data.map ->
  labelX = rok = parseInt it.rok, 10
  podilmaturantu = parseFloat it.mat
  labelY = "#{ig.utils.formatNumber podilmaturantu * 100, 1} %"
  x = scaleX rok
  y = scaleY podilmaturantu
  {rok, podilmaturantu, labelX, labelY, x, y}

data = [{points: podilmaturantu}]
width = 750
height = 300
padding = top: 15, right: 25, bottom: 25, left: 72
console.log container.node!
linechart = new ig.LineChart container, {data, width, height, padding}
