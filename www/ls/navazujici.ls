return unless ig.containers["navazujici"]
container = d3.select ig.containers["navazujici"]
container.append \h3
  ..html "Podíl studentů navazujících z bakaláře do magisterského studia"
data = d3.csv.parse ig.data.navazujici
scaleX = d3.scale.linear!
  ..domain [2001, 2014]
scaleY = d3.scale.linear!
  ..domain [0.92, 0]

navazujici = data.map ->
  labelX = rok = parseInt it.rok, 10
  navazujici = parseFloat it.nav
  labelY = "#{ig.utils.formatNumber navazujici * 100, 1} %"
  x = scaleX rok
  y = scaleY navazujici
  {rok, navazujici, labelX, labelY, x, y}

data = [{points: navazujici}]
width = 750
height = 300
padding = top: 15, right: 25, bottom: 25, left: 72
linechart = new ig.LineChart container, {data, width, height, padding}
