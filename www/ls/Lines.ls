class ig.Lines
  (@parentElement, @data) ->
    @element = @parentElement.append \div
      ..attr \class \lines
    width = 150px
    height = 150px
    padding = {top: 5 right: 20 bottom: 27 left: 47}
    @innerWidth = innerWidth = width - padding.left - padding.right
    @innerHeight = innerHeight = height - padding.top - padding.bottom

    @xScale = d3.scale.linear!
      ..domain [2000, 2013]
      ..range [0, innerWidth]

    barWidth = innerWidth / (@xScale.domain!.1 - @xScale.domain!.0)

    @data.forEach @~getSignificantPoints

    @yScale = y = d3.scale.linear!
      ..domain [8.3 53.2]
      ..range [innerHeight, 0]
    @path = d3.svg.line!
      ..x ~> @xScale it.x
      ..y ~> @yScale it.y
    @element.selectAll \.line .data @data .enter!append \div
      ..attr \class \line
      ..append \h3
        ..html (.title)
      ..append \div
        ..attr \class \horizontal-extent
      ..append \svg
        ..attr \width width
        ..attr \height height
        ..append \g
          ..attr \transform "translate(#{padding.left},#{padding.top})"
          ..attr \class \active-lines
          ..append \line
            ..attr \class \horizontal
            ..attr \x1 -13
          ..append \line
            ..attr \class \vertical
            ..attr \y2 innerHeight + 13
        ..append \g
          ..attr \class \drawing
          ..attr \transform "translate(#{padding.left},#{padding.top})"
          ..append \path
            ..attr \d ({data}, i) ~> @path data
          ..selectAll \circle.point .data (.data) .enter!append \circle
            ..attr \class \point
            ..classed \significant (d, i, ii) ~> d in @data[ii].significantYPoints
            ..attr \cx ({x}) ~> @xScale x
            ..attr \cy ({y}) ~> @yScale y
            ..attr \r 1.5
        ..append \g
          ..attr \class "axis x"
          ..attr \transform "translate(#{padding.left},#{height - 15})"
          ..append \line
            ..attr \class \full-extent
            ..attr \x1 -10
            ..attr \x2 innerWidth + padding.right
          ..append \line
            ..attr \class \extent
            ..attr \x1 (d, i) ~> @xScale d.data.0.x
            ..attr \x2 (d, i) ~> @xScale d.data[*-1].x
          ..selectAll \line.mark .data (.data) .enter!append \line
            ..attr \class \mark
            ..classed \significant (d, i, ii) ~> d in @data[ii].significantXPoints
            ..attr \x1 (d, i, ii) ~> @xScale d.x
            ..attr \x2 (d, i, ii) ~> @xScale d.x
            ..attr \y2 3
          ..selectAll \text.significant .data (.significantXPoints) .enter!append \text
            ..attr \class \significant
            ..attr \text-anchor \middle
            ..text (.label)
            ..attr \y 15
            ..attr \x (d, i, ii) ~> @xScale d.x
          ..append \text
            ..attr \class \active-text
            ..attr \text-anchor \middle
            ..attr \y 15
        ..append \g
          ..attr \class "axis y"
          ..attr \transform "translate(37,#{padding.top})"
          ..append \line
            ..attr \class \full-extent
            ..attr \y1 0
            ..attr \y2 innerHeight + 10
          ..append \line
            ..attr \class \extent
            ..attr \y1 (d, i) ~> @yScale d.yExtent.0
            ..attr \y2 (d, i) ~> @yScale d.yExtent.1
          ..selectAll \line.mark .data (.data) .enter!append \line
            ..attr \class \mark
            ..classed \significant (d, i, ii) ~> d in @data[ii].significantYPoints
            ..attr \x1 0
            ..attr \x1 -3
            ..attr \y1 (d, i, ii) ~> @yScale d.y
            ..attr \y2 (d, i, ii) ~> @yScale d.y
          ..selectAll \text.significant .data (.significantYPoints) .enter!append \text
            ..attr \class \significant
            ..text (d, i, ii) ~> @createText d, @data[ii]
            ..attr \y (d, i, ii) ~> @yScale d.y
            ..attr \dy 3
            ..attr \x -7
            ..attr \text-anchor \end
          ..append \text
            ..attr \class \active-text
            ..attr \dy 3
            ..attr \x -7
            ..attr \text-anchor \end
        ..append \g
          ..attr \transform "translate(#{padding.left},#{padding.top})"
          ..attr \class \interaction
          ..selectAll \rect .data (.data) .enter!append \rect
            ..attr \width barWidth
            ..attr \x (d) ~> (@xScale d.x) - barWidth / 2
            ..attr \height innerHeight + 30
            ..attr \y -5
            ..on \mouseover ~> @highlight it
            ..on \tochstart ~> @highlight it
            ..on \mouseout @~downlight
    @svg = @element.selectAll \svg
    @circles = @svg.selectAll \circle
    @specificLine = @svg.select \g.drawing .append \path
      ..attr \class \specific
    @activeLineHorizontal = @svg.selectAll ".active-lines .horizontal"
    @activeLineVertical   = @svg.selectAll ".active-lines .vertical"
    @activeTextX = @svg.selectAll ".axis.x text.active-text"
    @activeTextY = @svg.selectAll ".axis.y text.active-text"

  getSignificantPoints: (line) ->
    extent = d3.extent line.data.map (.y)
    min = max = line.data.0
    for datum in line.data
      max = datum if datum.y > max.y
      min = datum if datum.y < min.y
    line.yExtent = [min.y, max.y]
    line.significantYPoints = [max, min]
    line.significantXPoints = [line.data.0, line.data[*-1]]

  highlight: ({x, parent}) ->
    specificPath = @path parent.data
    @specificLine
      ..attr \d ->
        if it is parent
          ""
        else
          specificPath
    @svg
      ..classed \active yes
      ..classed \specific -> it is parent
    @circles.classed \active (.x == x)
    points = @data.map (line) ->
      line.data.filter (-> it.x == x) .pop! || null

    @activeLineHorizontal
      ..filter ((d, _, i) -> points[i])
        ..classed \active yes
        ..attr \x2 (d, _, i) ~> @xScale points[i].x
        ..attr \y1 (d, _, i) ~> @yScale points[i].y
        ..attr \y2 (d, _, i) ~> @yScale points[i].y

    @activeLineVertical
      ..filter ((d, _, i) -> points[i])
        ..classed \active yes
        ..attr \y1 (d, _, i) ~> @yScale points[i].y
        ..attr \x1 (d, _, i) ~> @xScale points[i].x
        ..attr \x2 (d, _, i) ~> @xScale points[i].x

    @activeTextX
      ..filter ((d, _, i) -> points[i])
        ..classed \active yes
        ..attr \x (d, _, i) ~>
          xCoord = @xScale x
          if x == 17
            xCoord -= 7
          xCoord
        ..text (d, _, i) -> points[i].label
    @activeTextY
      ..filter ((d, _, i) -> points[i])
        ..classed \active yes
        ..attr \y (d, _, i) ~> @yScale points[i].y
        ..text (d, _, i) ~> @createText points[i], @data[i]

  downlight: ->
    @element
      .selectAll \.active
      .classed \active no
      .classed \specific no
    @specificLine.attr \d ""

  createText: (point, line) ->
    decimals =
      | point.y > 100 => 0
      | point.y > 10 => 1
      | otherwise => 2
    ig.utils.formatNumber point.y, decimals
