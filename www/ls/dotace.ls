(i) <~ [1, 2].forEach
return unless ig.containers["dotace-#i"]
container = d3.select ig.containers["dotace-#i"]
data = d3.csv.parse ig.data.dotace, (row) ->
  for field in <[rok studentu penez]>
    row[field] = parseFloat row[field]
  row

data.length = 14

studentuY = d3.scale.linear!
  ..domain [0 395990]
  ..range [0 100]

penezY = d3.scale.linear!
  ..domain [0 21903501]
  ..range [0 100]

container.append \h3
  ..html "Počty studujících a dotace VŠ"

container.selectAll \div.year .data data .enter!append \div
  ..attr \class \year
  ..append \div
    ..attr \class \bar-container
    ..append \div
      ..attr \class "bar studenti"
      ..style \height -> "#{studentuY it.studentu}%"
      ..append \div
        ..attr \class \label
        ..html -> "#{ig.utils.formatNumber it.studentu} studentů"
    ..append \div
      ..attr \class "bar penize"
      ..style \height -> "#{penezY it.penez}%"
      ..append \div
        ..attr \class \label
        ..html -> "#{ig.utils.formatNumber it.penez * 1e3} Kč"
  ..append \div
    ..attr \class "label label-year"
    ..html (.rok)
  ..append \div
    ..attr \class "label label-ratio"
    ..html -> "#{ig.utils.formatNumber it.penez / it.studentu * 1e3} Kč na 1 studenta"
