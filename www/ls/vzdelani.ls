years = [2000 2005 2006 2007 2008 2009 2010 2011 2012 2013]
width = 100
height = 100

data = d3.csv.parse ig.data.vzdelani, (row) ->
  row.title = row.stat
  row.data = for year in years
    value = parseFloat row[year]
    x = year
    y = value
    label = year
    parent = row
    {year, value, x, y, label, parent}
  row.max = d3.max row.data.map (.y)
  row
(i) <~ [1, 2].forEach
container = ig.containers["vzdelani-#i"]
return unless container
container = d3.select container
container.append \h3
  ..html "Absolventi vysokých škol v populaci"
lines = new ig.Lines container, data
if i == 1
  lines
    ..downlight = -> lines.highlight data[0].data[0]
    ..highlight data[0].data[0]
