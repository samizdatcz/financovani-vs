class DividedLines extends ig.Lines
  getSignificantPoints: (line) ->
    super line
    line.significantXPoints = [line.data.0, line.data.9]

(i) <~ [1, 2].forEach
container = ig.containers["studenti-na-ucitele-#i"]
return unless container
container = d3.select container
container.append \h3
  ..html "Počet studentů na jednoho učitele"
years = [2000 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013]
width = 100
height = 100

data = d3.csv.parse ig.data['studenti-na-ucitele'], (row) ->
  row.title = row.stat
  row.data = for year in years
    continue if row[year] == "NA"
    value = parseFloat row[year]
    x = year
    y = value
    label = year
    parent = row
    {year, value, x, y, label, parent}
  row.max = d3.max row.data.map (.y)
  row
if i == 1
  lines = new DividedLines container, data
    ..element.selectAll \g.drawing .append \rect
      ..attr \width 5 + lines.innerWidth - lines.xScale 2010
      ..attr \x 2 + lines.xScale 2010
      ..attr \height lines.innerHeight + 12
      ..attr \fill "rgba(255,255,255,0.8)"
else
  lines = new ig.Lines container, data
