(i) <~ [1, 2].forEach
return unless ig.containers["prijati-#i"]
container = d3.select ig.containers["prijati-#i"]
container.append \h3
  ..html "Počet <span>devatenáctiletých</span> a <span>studentů přijatých na vysokou školu</span>"
data = d3.csv.parse ig.data.prijati
scaleX = d3.scale.linear!
  ..domain [1999, 2014]
scaleY = d3.scale.linear!
  ..domain [141206, 0]

prijati = data.map ->
  labelX = rok = parseInt it.rok, 10
  prijati = parseInt it.prijati, 10
  labelY = ig.utils.formatNumber prijati
  x = scaleX rok
  y = scaleY prijati
  {rok, prijati, labelX, labelY, x, y}
devatenatileti = data
  .filter ->
    it.devatenatileti.length
  .map ->
    labelX = rok = parseInt it.rok, 10
    devatenatileti = parseInt it.devatenatileti, 10
    labelY = ig.utils.formatNumber devatenatileti
    x = scaleX rok
    y = scaleY devatenatileti
    {rok, devatenatileti, labelX, labelY, x, y}

data = [{points: prijati}, {points: devatenatileti}]
width = 750
height = 300
padding = top: 15, right: 25, bottom: 25, left: 72
linechart = new ig.LineChart container, {data, width, height, padding}
if i == 1
  shade = container.append \div
    ..attr \class \shade
    ..style \left "#{5 + linechart.padding.left + linechart.scaleX scaleX 2010}px"
else if i == 2
  linechart.highlight data.0.points.11
  linechart.downlight = ->
    linechart.highlight data.0.points.11
