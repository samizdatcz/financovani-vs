(table) <~ <[kriteria koeficient]>.forEach
return unless ig.containers[table]
container = d3.select ig.containers[table]
data = d3.csv.parse ig.data[table]
headers = for key of data.0 => key
valueHeaders = headers.slice 1
values = []
toValue = -> parseFloat it.replace "," "."
for row in data
  for header in valueHeaders
    values.push toValue row[header]
scale = d3.scale.linear!
  ..domain [0, d3.max values]
  ..range [0 100]
container.append \table
  ..append \thead
    ..append \tr .selectAll \th .data headers .enter!append \th
      ..html -> it
  ..append \tbody
    ..selectAll \tr .data data .enter!append \tr
      ..selectAll \td .data headers .enter!append \td
        ..html (header, _, i) -> data[i][header]
