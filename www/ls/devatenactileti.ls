return unless ig.containers["devatenatileti"]
container = d3.select ig.containers["devatenatileti"]
container.append \h3
  ..html "Počet devatenáctiletých"
data = d3.csv.parse ig.data.devatenatileti
scaleX = d3.scale.linear!
  ..domain [2000, 2017]
scaleY = d3.scale.linear!
  ..domain [141206, 0]

devatenatileti = data.map ->
  labelX = rok = parseInt it.rok, 10
  devatenatileti = parseFloat it['19letych']
  labelY = "#{ig.utils.formatNumber devatenatileti}"
  x = scaleX rok
  y = scaleY devatenatileti
  {rok, devatenatileti, labelX, labelY, x, y}
devatenatileti.length = 18
data = [{points: devatenatileti}]
width = 750
height = 300
padding = top: 15, right: 25, bottom: 25, left: 72
linechart = new ig.LineChart container, {data, width, height, padding}
